# godot-x11

This is heavily inspired from Heavily inspired from [barichello/godot-ci](https://hub.docker.com/r/barichello/godot-ci).
It should be suitable for general use involving [Godot](https://godotengine.org).
I typically use it to run CI scripts on [Gitlab](https://gitlab.com).
