use godot::prelude::*;

mod copyright;
pub use copyright::*;

struct MyExtension;

#[gdextension]
unsafe impl ExtensionLibrary for MyExtension {}
