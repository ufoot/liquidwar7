liquidwar7core
==============

[Liquidwar7](https://ufoot.org/liquidwar/v7) is a distributed wargame.
It is developped with [Godot](https://godotengine.org) and [godot-rust](https://godot-rust.github.io/).

This is the core logic library, low-level things which are game-engine agnostic.

![Liquid War 7 icon](https://gitlab.com/ufoot/liquidwar7/raw/main/liquidwar7.jpg)

Status
------

Work in progress, no game, no nothing.

[![Build Status](https://gitlab.com/ufoot/liquidwar7/badges/main/pipeline.svg)](https://gitlab.com/ufoot/liquidwar7/pipelines)
[![Crates.io](https://img.shields.io/crates/v/liquidwar7core.svg)](https://crates.io/crates/liquidwar7core)
[![Docs](https://docs.rs/liquidwar7core/badge.svg)](https://docs.rs/liquidwar7core)

Links
-----

* [crate](https://crates.io/crates/liquidwar7core) on crates.io
* [doc](https://docs.rs/liquidwar7core/) on docs.rs
* [source](https://gitlab.com/ufoot/liquidwar7/tree/main/rust/liquidwar7core) on gitlab.com

License
-------

Copyright (C)  2018, 2019, 2020, 2021  Christian Mauduit [<ufoot@ufoot.org>](mailto:ufoot@ufoot.org)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it wil/l be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [www.gnu.org/licences](https://www.gnu.org/licenses/).

Liquid War 7 homepage: [gitlab.com/ufoot/liquidwar/v7](https://gitlab.com/ufoot/liquidwar7)

Contact author: [ufoot@ufoot.org](mailto:ufoot@ufoot.org)
