Liquid War 7
============

[Liquid War 7](https://gitlab.com/ufoot/liquidwar/v7) is a distributed wargame.
It is developped with [Godot](https://godotengine.org) and maybe [godot-rust](https://godot-rust.github.io/).

![Liquid War 7 icon](https://gitlab.com/ufoot/liquidwar7/raw/main/liquidwar7.jpg)

Status
------

Work in progress, no game, no nothing.

[![Build Status](https://gitlab.com/ufoot/liquidwar7/badges/main/pipeline.svg)](https://gitlab.com/ufoot/liquidwar7/pipelines)

Documentation
-------------

This is really experimental, for working (previous) versions, see:

* [Liquid War 5](https://ufoot.org/liquidwar/v5) finished, stable, but 20+ years old, do not expect a visually stunning game.
* [Liquid War 6](https://www.gnu.org/software/liquidwar6/) did not fullfil expectations, it is workable but requires many improvements still.

So why launch a new version now ? Well, [Godot](https://godotengine.org) really changes the deal and makes it dead simple to craft visually acceptable, portable programs, in no times compared to the technical paths I explored with previous versions.

If you are interested, please [ping me](mailto:ufoot@ufoot.org) there is a [Slack channel](https://liquidwar.slack.com) to chat about the game in general, I can invite you. The old [mailing list](http://mail.nongnu.org/mailman/listinfo/liquidwar-user) (see its [archives](http://mail.nongnu.org/pipermail/liquidwar-user/)) is still there, I watch it, but do not expect high traffic.

Authors
-------

* Christian Mauduit [<ufoot@ufoot.org>](mailto:ufoot@ufoot.org) : main developper, project
  maintainer.

License
-------

Copyright (C)  2018, 2019, 2020, 2021  Christian Mauduit [<ufoot@ufoot.org>](mailto:ufoot@ufoot.org)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it wil/l be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [www.gnu.org/licences](https://www.gnu.org/licenses/).

Liquid War 7 homepage: [gitlab.com/ufoot/liquidwar/v7](https://gitlab.com/ufoot/liquidwar7)

Contact author: [ufoot@ufoot.org](mailto:ufoot@ufoot.org)
