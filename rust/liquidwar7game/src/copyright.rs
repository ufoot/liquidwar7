use godot::prelude::*;

use godot::engine::ILabel;
use godot::engine::Label;

#[derive(GodotClass)]
#[class(base=Label)]
pub struct Copyright {
    base: Base<Label>,
}

#[godot_api]
impl ILabel for Copyright {
    fn init(base: Base<Label>) -> Self {
        //        godot_print!("Hello, world!"); // Prints to the Godot console

        //let mut label = Self { base };
        //label.base_mut().set_text(GString::from("ooops"));
        //label
        Self { base }
    }

    fn process(&mut self, _delta: f64) {
        self.base_mut().set_text(GString::from("ooops"));
    }
}

// impl Copyright {
//     fn new(_owner: &Label) -> Self {
//         Copyright
//     }

//     #[export]
//     fn _ready(&self, owner: &Label) {
//         owner.set_text("Liquid War 7 (c) 2018-2024 Christian Mauduit");
//     }
// }
