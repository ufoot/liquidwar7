#!/bin/sh

DISPLAY=:42
echo "Starting Xfvb on ${DISPLAY}"
Xvfb $DISPLAY -ac -audit 5 -pixdepths 1 8 16 24 32 -screen 0 1280x720x24 -fbdir . &
sleep 1
