GRCC_GAME_PKG_NAME=liquidwar7
GRCC_GAME_PKG_VERSION=0.1.0
GRCC_GODOT_RUST_LIB_NAME=liquidwar7game
include grcc.mk

all: grcc-all

test: grcc-test

clean: grcc-clean

doc: grcc-doc

native: grcc-native

cross: grcc-cross

export: grcc-export

windows: grcc-pkg-windows

android: grcc-pkg-android

macosx: grcc-pkg-macosx

linux: grcc-pkg-linux
